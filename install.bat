@echo off
echo Setting up virtual environment...


python -m venv venv


venv\Scripts\activate


python -m pip install --upgrade pip


pip install --upgrade wheel


pip install -r requirements.txt

echo Installation complete.
